/**
 * @file
 * Javascript for the Geofield Map widget.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  // Override existing geofieldMapInit behaviors
  Drupal.behaviors.geofieldMapInit = {
    attach: function (context, drupalSettings) {

      // Init all maps in drupalSettings.
      $.each(drupalSettings['geofield_map'], function (mapid, options) {

        // Define the first map id, for a multivalue geofield map.
        if (mapid.indexOf('0-value') !== -1) {
          Drupal.geoFieldMap.firstMapId = mapid;
        }
        // Check if the Map container really exists and hasn't been yet initialized.
        if ($('#' + mapid, context).length > 0 && !Drupal.geoFieldMap.map_data[mapid]) {

          // Set the map_data[mapid] settings.
          Drupal.geoFieldMap.map_data[mapid] = options;

          // Google maps library shouldn't be requested if the following
          // conditions apply:
          // - leaflet js is the chosen map library;
          // - geocoder integration is enabled;
          let map_library = options.map_library;
          if (map_library === 'leaflet') {
            // Check and wait for the Leaflet module to be loaded.
            let checkLeafletExist = setInterval(function () {
              if (window.hasOwnProperty('L')) {
                Drupal.geoFieldMap.map_initialize(options, context);
                clearInterval(checkLeafletExist);
              }
            }, 100);
          } else if (map_library === 'gmap') {
            // Load before the Gmap Library, if needed, then initialize the Map.
            Drupal.geoFieldMap.loadGoogle(mapid, options.gmap_api_key, function () {
              Drupal.geoFieldMap.map_initialize(options, context);
            });
          } else {
            // Load map library resources if needed, then initialize the Map.
            if (Drupal.geoFieldMap.ext) {
              if (Drupal.geoFieldMap.ext[map_library]) {
                Drupal.geoFieldMap.ext[map_library].load(mapid, options.ext[map_library].settings,
                  function () {
                    Drupal.geoFieldMap.ext[map_library].map_initialize(options);
                  }
                );
              } else {
                window.alert("Map library '" + map_library + "' not found!");
              }
            } else {
              window.alert("Map library '" + map_library + "' not available!");
            }
          }
          // TODO: handle Google GeoCoder init?
        }
      });

    }
  };

  Drupal.geoFieldMap.getLatLng_original = Drupal.geoFieldMap.getLatLng;
  Drupal.geoFieldMap.getLatLng = function (mapid, lat, lng) {
    let self = this;
    let position;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        position = Drupal.geoFieldMap.getLatLng_original(mapid, lat, lng);
        break;
      default:
        position = Drupal.geoFieldMap.ext[map_library].getLatLng(mapid, lat, lng);
    }
    return position;
  };

  Drupal.geoFieldMap.trigger_geocode_original = Drupal.geoFieldMap.trigger_geocode;
  Drupal.geoFieldMap.trigger_geocode = function (mapid, position) {
    let self = this;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        Drupal.geoFieldMap.trigger_geocode_original(mapid, position);
        break;
      default:
        Drupal.geoFieldMap.ext[map_library].trigger_geocode(mapid, position);
    }
  };

  Drupal.geoFieldMap.getMarkerPosition_original = Drupal.geoFieldMap.getMarkerPosition;
  Drupal.geoFieldMap.getMarkerPosition = function (mapid) {
    let self = this;
    let position;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        position = Drupal.geoFieldMap.getMarkerPosition_original(mapid);
        break;
      default:
        position = Drupal.geoFieldMap.ext[map_library].getMarkerPosition(mapid);
    }
    return position;
  };

  Drupal.geoFieldMap.mapSetCenter_original = Drupal.geoFieldMap.mapSetCenter;
  Drupal.geoFieldMap.mapSetCenter = function (mapid, position) {
    let self = this;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        Drupal.geoFieldMap.mapSetCenter_original(mapid, position);
        break;
      default:
        Drupal.geoFieldMap.ext[map_library].mapSetCenter(mapid, position);
    }
  };

  Drupal.geoFieldMap.setLatLngValues_original = Drupal.geoFieldMap.setLatLngValues;
  Drupal.geoFieldMap.setLatLngValues = function (mapid, position) {
    let self = this;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        Drupal.geoFieldMap.setLatLngValues_original(mapid, position);
        break;
      default:
        Drupal.geoFieldMap.ext[map_library].setLatLngValues(mapid, position);
    }
  };

  Drupal.geoFieldMap.reverse_geocode_original = Drupal.geoFieldMap.reverse_geocode;
  Drupal.geoFieldMap.reverse_geocode = function (mapid, position) {
    let self = this;
    let map_library = self.map_data[mapid].map_library;
    switch (map_library) {
      case 'leaflet':
      case 'gmap':
        Drupal.geoFieldMap.reverse_geocode_original(mapid, position);
        break;
      default:
        Drupal.geoFieldMap.ext[map_library].reverse_geocode(mapid, position);
    }
  };

})(jQuery, Drupal, drupalSettings);
