<?php

namespace Drupal\geofield_map_ext\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield_map\Element\GeofieldMap;

/**
 * Provides a Geofield Map Ext form element.
 *
 * @FormElement("geofield_map_ext")
 */
class GeofieldMapExt extends GeofieldMap {

  /**
   * The Map Library Plugin Manager service.
   *
   * @var \Drupal\geofield_map_ext\MapLibraryPluginManager
   */
  protected static $mapLibraryManager;

  /**
   * Generates the Geofield Map form element.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element. Note that $element must be taken by reference here, so processed
   *   child elements are taken over into $form_state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function latLonProcess(array &$element, FormStateInterface $form_state, array &$complete_form) {

    $element = parent::latLonProcess($element, $form_state, $complete_form);

    // Use attributes for potential TacJS handling.
    $element['map']['geofield_map']['#attributes'] = [
      'id' => $element['#mapid'],
      'class' => 'geofield-map-widget',
      'data-map-library' => $element['#map_library'],
    ];

    $element['#attached']['library'][] = 'geofield_map_ext/geofield_map_ext_widget';

    if (self::getMapLibraryManager()->hasDefinition($element['#map_library'])) {
      /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
      $map_library_plugin = self::getMapLibraryManager()->createInstance($element['#map_library']);
      if (isset($map_library_plugin)) {
        $map_library_plugin->mapFormElement($element);
      }
    }

    return $element;
  }

  /**
   * Fetchs the Map Library Plugin Manager service.
   * @return \Drupal\geofield_map_ext\MapLibraryPluginManager|mixed
   */
  protected static function getMapLibraryManager() {
    if (empty(self::$mapLibraryManager)) {
      self::$mapLibraryManager = \Drupal::service('plugin.manager.map_library_plugin');
    }
    return self::$mapLibraryManager;
  }

}
