<?php

namespace Drupal\geofield_map_ext;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the map libraries plugin plugin manager.
 */
class MapLibraryPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new MapLibraryPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/MapLibrary', $namespaces, $module_handler, 'Drupal\geofield_map_ext\MapLibraryPluginInterface', 'Drupal\geofield_map_ext\Annotation\MapLibraryPlugin');

    $this->alterInfo('geofield_map_ext_library_plugin_info');
    $this->setCacheBackend($cache_backend, 'geofield_map_ext_library_plugin_plugins');
  }

  /**
   * Get the associative array of all defined Map Libraries.
   */
  public function getMapLibraries() {

    $map_libraries = [];
    foreach ($this->getDefinitions() as $k => $plugin) {
      $map_libraries[$k] = [
        'label' => $plugin['label'],
      ];
    }
    ksort($map_libraries);
    return $map_libraries;
  }

  /**
   * Get the id => label associative array of all defined Map Libraries.
   */
  public function getMapLibrariesOptions() {
    $options = [];
    foreach ($this->getDefinitions() as $k => $plugin) {
      $options[$k] = $plugin['label'];
    }
    ksort($options);
    return $options;
  }

}
